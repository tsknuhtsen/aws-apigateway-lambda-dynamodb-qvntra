if [[ -f lambda.zip ]]; then rm lambda.zip; else echo "create deployment package:"; fi
mkdir -p tmp
cp lambda/* tmp
cd tmp
if [[ -f requirements.txt ]]; then pip install --upgrade -t . -r requirements.txt; else echo "no dependency"; fi
zip -u -r9 ../lambda.zip .
cd ..
rm -rf tmp

# upload deployment package to s3 bucket
#account_id=$(aws sts get-caller-identity | jq -r '.Account')
#bucket_name="$account_id-qvntra-lambda-deployment-package"
#if aws s3 ls "s3://$bucket_name" 2>&1 | grep -q 'An error occurred'
#then
#    echo "bucket does not exist or permission error.";
#    echo "create bucket";
#else
#    echo "bucket exist";
#    aws s3 cp --debug lambda.zip "s3://$bucket_name/lambda.zip";
#fi