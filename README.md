# qvntra API

![Architecture Diagram](architecture.png)


## API Description

API is described in the swagger file [swagger.yml](swagger.yml)


## Deploy

* terraform requirement (minimal version v0.13)

* setup python environment
```bash
# install virtualenv
# create python=3.7 virtual environment called qvntra

source qvntra/bin/activate
```

* build lambda deployment package

```bash
chmod +x build_lambda.sh
./build_lambda.sh
```

* deploy infrastructure

```bash
terraform plan
terraform apply
```

* test
automated test for apis, api_endpoint should be provided as parameter
```bash
./test_api.sh $api_endpoint
```
api_endpoint can be found in terraform apply output


## Details

#### About lastseen and recent payload

Since the computation complexity and practical needs a tradeoff is taken here, which means define a window for lastseen and recent payload. Only data within this window is considered
meaningful, if no data is found within this window NaN will be returned.

For lastseen it is 7 hours, for recent_payload it is 24 hours. Lower the window size will increase performance but deminish search coverage.

* Error Handling Patterns in Amazon API Gateway and AWS Lambda: https://aws.amazon.com/blogs/compute/error-handling-patterns-in-amazon-api-gateway-and-aws-lambda/
* Serverless Applications with AWS Lambda and API Gateway: https://learn.hashicorp.com/terraform/aws/lambda-api-gateway

#### About dynamodb cache table

* time_stamp !=0 are entries for baseline
* time_stamp =0 (0 is dummy value) are entries for batch(weekly, monthly) average


#### List of Issues

* lambda deployment package could be too large for local machine deployment (this can be fixed nicely in terraform 0.13 using module dependency and lambda s3 bucket deployment)
* potential bottleneck on update baseline lambda
* deployment error "Error: rpc error: code = Unavailable desc = transport is closing" (just retry), a potential terraform bug
