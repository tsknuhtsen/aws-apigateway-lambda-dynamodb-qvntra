import pandas as pd
import numpy as np
from helper import *
import json
import time


def is_recent_payload(row :dict):
    topic = row.get("topic")
    status = float(row.get("status"))
    prediction = row.get("prediction")
    occupancy = row.get("occupancy")
    heart_rate = row.get("heart_rate")

    if topic == "sensor_bedroom" and heart_rate != 0:
        return True
    elif topic == "sensor_bathroom" and not pd.isna(prediction):
        return True
    elif topic == "sensor_kitchen" and occupancy == True:
        return True
    elif topic == "sensor_livingroom" and occupancy == True:
        return True
    else:
        return False


def lambda_handler(event, context):
    client = boto3.client("dynamodb", region_name="us-east-1")
    # get parameter
    print(event)
    user_id = event["queryStringParameters"].get("user_id")

    # restrict recent to the last 12 hours, if no payload is found in thisv time range NaN will be returned
    starttime = int(time.time())*1000
    lastseen_window_size = 12
    endtime = starttime - lastseen_window_size * 3600000

    projection_expression = "topic, time_stamp, prediction, #status, occupancy, heart_rate"
    expression_attribute_names = {"#status": "status"}
    data = get_data_by_user_id( client, 
                                user_id, 
                                starttime, 
                                endtime, 
                                projection_expression, 
                                expression_attribute_names)

    for index, row in data.iterrows():
        if is_recent_payload(row): 
            body = json.loads(row.to_json())
            break
    else:
        body = {}

    return {
        'statusCode': 200,
        'headers': {'Content-Type': 'application/json'},
        'body': body
    }

if __name__=="__main__":
    event = {}
    event["queryStringParameters"] = {"user_id":"thyge"}
    result = lambda_handler(event, {})
    print(result)
