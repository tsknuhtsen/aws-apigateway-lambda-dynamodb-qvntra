import json
import pandas as pd
import numpy as np
from helper import *

def verify_parameter(user_id, period, sensor):
    if (user_id is None):
        raise ValueError("Bad Parameter: user_id")
    if period not in ("1D", "1W", "1M") or (period is None):
        raise ValueError("Bad Parameter: period")
    if sensor not in ("heart_rate", "respiration_rate", "gait_velocity", "prediction", "prediction_event", "presence") or (sensor is None):
        raise ValueError("Bad Parameter: sensor")

def lambda_handler(event, context):
    # get parameter
    user_id = event["queryStringParameters"].get("user_id")
    period = event["queryStringParameters"].get("period")
    # heart_rate, respiratory_rate, gait_velocity
    sensor = event["queryStringParameters"].get("sensor")
    # verify parameter
    verify_parameter(user_id, period, sensor)


    client = boto3.client("dynamodb", region_name="us-east-1")

    starttime = int(time.time())*1000
    if period == "1D":
        endtime = get_beginning_of_day_unix_timestamp()
    if period == "1W":
        endtime = get_beginning_of_week_unix_timestamp()
    if period == "1M":
        endtime = get_beginning_of_month_unix_timestamp()

    if sensor == "prediction_event":
        projection_expression = "start_timestamp, end_timestamp, #sensor"
    else:
        projection_expression = "time_stamp, #sensor"


    expression_attribute_names = {"#sensor": sensor}

    data = get_data_by_user_id(client, user_id, starttime, endtime, projection_expression, expression_attribute_names)
    # data processing logic
    # if there is sensor data, process furthur
    if sensor in data.columns:
        # remove na value from the return datapoints
        data = data.dropna(subset=[sensor])
        # feature: groupby days
        # prediction_event and prediction does not need this feature
        if sensor in ["prediction_event", "presence", "prediction"]:
            pass
        elif sensor == "gait_velocity":
            data["time_stamp"] = pd.to_datetime(data["time_stamp"], unit="ms")
            data = data.groupby(by=data["time_stamp"].dt.date).mean()
            data = data.reset_index()
            data["time_stamp"] = data["time_stamp"].astype(str)
            #remove - from timestamp and converting it to int
            data.time_stamp = data['time_stamp'].str.replace('-','')
            data.time_stamp = data.time_stamp.astype(int)
            #below line checks if "gait_velocity" exists in the dataframe and rename to "avg", if present
            data.columns = ['avg' if x==sensor else x for x in data.columns]

        # sensor is heart_rate and respiration_rate
        # filter out 0 value
        else:
            data = data[data[sensor]!=0]
            data["time_stamp"] = pd.to_datetime(data["time_stamp"], unit="ms")
            data = data.groupby(by=data["time_stamp"].dt.date).mean()
            data = data.reset_index()
            data["time_stamp"] = data["time_stamp"].astype(str)
            #remove - from timestamp and converting it to int
            data.time_stamp = data['time_stamp'].str.replace('-','')
            data.time_stamp = data.time_stamp.astype(int)
            #below line checks if other sensor names exists in the dataframe and rename to "avg", if present
            data.columns = ['avg' if x==sensor else x for x in data.columns]
    # if there are no sensor data, empty dataframe
    else:
        data = pd.DataFrame()


    body = data.to_dict("records")
    return {
        'statusCode': 200,
        'headers': {'Content-Type': 'application/json'},
        'body': body
    }
    return body

# for local invoke testing
if __name__ == "__main__":
    event = {}
    event["queryStringParameters"] = {}
    event["queryStringParameters"]["user_id"] = "thyge"
    event["queryStringParameters"]["period"] = "1W"
    event["queryStringParameters"]["sensor"] = "respiration_rate"
    response = lambda_handler(event, {})
    # print(response["body"])
    data = response["body"]
    print(data)
