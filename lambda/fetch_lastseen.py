import json
import pandas as pd
import numpy as np
from helper import *

def get_first_nonzero_notnull_value(s:pd.Series):
    for index, value in s.items():
        if not pd.isna(value) and value!=0 : 
            return value
    return np.nan
    
def lambda_handler(event, context):
    # get parameter
    user_id = event["queryStringParameters"].get("user_id")

    client = boto3.client("dynamodb", region_name="us-east-1")

    # restrict recent to the last 7 hours, if no payload is found in thisv time range NaN will be returned
    starttime = int(time.time())*1000
    lastseen_window_size = 7
    endtime = starttime - lastseen_window_size * 3600 * 1000

    
    projection_expression = "topic, time_stamp, respiration_rate, heart_rate, gait_velocity, prediction, prediction_event"
    data = get_data_by_user_id(client, user_id, starttime, endtime, projection_expression)
    print(data[data["topic"]=="sensor_bathroom_events"])
    if not data.empty:

        gait_velocity = get_first_nonzero_notnull_value(data["gait_velocity"]) if "gait_velocity" in data.columns else np.nan
        heart_rate = get_first_nonzero_notnull_value(data["heart_rate"]) if "heart_rate" in data.columns else np.nan
        respiration_rate = get_first_nonzero_notnull_value(data["respiration_rate"]) if "respiration_rate" in data.columns else np.nan

        end_time = max(data["time_stamp"])
        start_time = end_time - 1*3600*1000

        if "prediction" in data.columns:
            prediction = data["prediction"].dropna()
            prediction_count = prediction[prediction.shift() != prediction].count()
        else:
            prediction_count = np.nan
        
        if "prediction_event" in data.columns:
            prediction_event = data["prediction_event"].dropna()
            prediction_event_count = prediction_event[prediction_event.shift() != prediction_event].count()
        else:
            prediction_event_count = np.nan

        body = {
            "gait_velocity":float(gait_velocity),
            "heart_rate":float(heart_rate),
            "respiration_rate":float(respiration_rate),
            "prediction":float(prediction_count),
            "prediction_event":float(prediction_event_count)
        }
        
        return {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json'},
            'body': body
        }


    else:
        return {
            "statusCode": 404,
            "headers": {'Content-Type': 'application/json'},
            "body": json.dumps({ "error": f"No Data Available for user {user_id} within last {lastseen_window_size} hours"}),
        }

# for local invoke testing
if __name__ == "__main__":
    event = {}
    event["queryStringParameters"] = {}
    event["queryStringParameters"]["user_id"] = "thyge"
    response = lambda_handler(event, {})
    print(response)
