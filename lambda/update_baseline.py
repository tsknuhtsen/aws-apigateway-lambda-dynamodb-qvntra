import json
import pandas as pd
import numpy as np
from helper import *
from dynamodb_json import json_util
import time

def calculate_baseline(data: pd.DataFrame):
    """ Calculate baseline for a certain user_id
    """
    initial_time = min(data["time_stamp"])
    endtime = initial_time + 7*3600*1000
    filtered_data = data[(data["time_stamp"]>=initial_time) & (data["time_stamp"]<=endtime)]
    # not all measures are included for all users
    if "gait_velocity" in filtered_data.columns:
        gait_velocity_avg = filtered_data["gait_velocity"].apply(lambda x: np.nan if x==0 else x).astype("float").mean(skipna=True)
    else:
        gait_velocity_avg = np.nan
    if "heart_rate" in filtered_data.columns:
        heart_rate_avg = filtered_data["heart_rate"].apply(lambda x: np.nan if x==0 else x).astype("float").mean(skipna=True)
    else:
        heart_rate_avg = np.nan
    if "respiration_rate" in filtered_data.columns:
        respiration_rate_avg = filtered_data["respiration_rate"].apply(lambda x: np.nan if x==0 else x).astype("float").mean(skipna=True)
    else:
        respiration_rate_avg = np.nan
    if "prediction" in filtered_data.columns:
        prediction = filtered_data["prediction"].dropna()
        prediction_count = prediction[prediction.shift() != prediction].count()
    else:
        prediction_count = np.nan
    if "prediction_event" in filtered_data.columns:
        prediction_event = filtered_data["prediction_event"].dropna()
        prediction_event_count = prediction_event[prediction_event.shift() != prediction_event].count()
    else:
        prediction_event_count = np.nan
    result = {
        "initial_time": initial_time,
        "gait_velocity": float(gait_velocity_avg),
        "heart_rate": float(heart_rate_avg),
        "respiration_rate": float(respiration_rate_avg),
        "prediction": float(prediction_count),
        "prediction_event": float(prediction_event_count)
    }
    return result



def lambda_handler(event, context):
    client = boto3.client("dynamodb", region_name="us-east-1")
    # all_user_ids = get_all_user_ids(client)
    all_user_ids = ["thyge"]
    # calculate the difference
    user_ids = all_user_ids

    starttime = int(time.time())*1000
    # infinite backwards into history
    window_size = 10000000000
    endtime = starttime - window_size * 3600000
    for user_id in user_ids:
        projection_expression = "topic, time_stamp, respiration_rate, heart_rate, gait_velocity, prediction, prediction_event"
        data = get_data_by_user_id(client, user_id, starttime, endtime, projection_expression)
        print(data["prediction_event"])
        # calculate baseline
        result = calculate_baseline(data)
        initial_time = str(result["initial_time"])
        baseline_gait_velocity = str(result["gait_velocity"])
        baseline_heart_rate = str(result["heart_rate"])
        baseline_respiration_rate = str(result["respiration_rate"])
        baseline_prediction = str(result["prediction"])
        baseline_prediction_event = str(result["prediction_event"])
        # put or update item
        expression_attribute_values = {}
        expression_attribute_values[":baseline_gait_velocity"] = {"S": baseline_gait_velocity}
        expression_attribute_values[":baseline_heart_rate"] = {"S": baseline_heart_rate}
        expression_attribute_values[":baseline_respiration_rate"] = {"S": baseline_respiration_rate}
        expression_attribute_values[":baseline_prediction"] = {"S": baseline_prediction}
        expression_attribute_values[":baseline_prediction_event"] = {"S": baseline_prediction_event}

        result_key = {}
        result_key["user_id"] = {"S": user_id}
        result_key["time_stamp"] = {"S": initial_time}

        client.update_item(
            TableName="qvntra-baseline-cache", 
            Key=result_key,
            UpdateExpression="set baseline_gait_velocity =:baseline_gait_velocity, \
                                baseline_heart_rate =:baseline_heart_rate, \
                                baseline_respiration_rate =:baseline_respiration_rate, \
                                baseline_prediction =:baseline_prediction, \
                                baseline_prediction_event =:baseline_prediction_event",
            ExpressionAttributeValues = expression_attribute_values
            )


# for local invoke testing
if __name__ == "__main__":
    event = {}
    response = lambda_handler(event, {})
    print(response["body"])
