import json
import pandas as pd
import numpy as np
from helper import *
from dynamodb_json import json_util
import time
from datetime import datetime
import os

def lambda_handler(event, context):
    client = boto3.client("dynamodb",
                          region_name="us-east-1")
    all_user_ids = get_all_user_ids(client)

    # calculate the difference
    user_ids = all_user_ids
    print(user_ids)
    projection_expression = "topic, time_stamp, respiration_rate, heart_rate, gait_velocity, prediction_event, presence, prediction"
    starttime = int(time.time())*1000

    for user_id in user_ids:
        monthly_endtime = get_beginning_of_month_unix_timestamp()

        # get monthly data
        data = get_data_by_user_id(client, user_id, starttime, monthly_endtime, projection_expression)

        # calculate monthly sensor average
        avg_gait_velocity_monthly = data[data["time_stamp"]>=monthly_endtime]["gait_velocity"].mean() if "gait_velocity" in data.columns else np.nan
        avg_heart_rate_monthly = data[ (data["time_stamp"]>=monthly_endtime) & (data["heart_rate"]!=0) ]["heart_rate"].mean() if "heart_rate" in data.columns else np.nan
        avg_respiration_rate_monthly = data[ (data["time_stamp"]>=monthly_endtime) & (data["respiration_rate"]!=0)]["respiration_rate"].mean() if "respiration_rate" in data.columns else np.nan

        ### Calculating the averages for presence and prediction_event sensors
        if "prediction_event" in data.columns:
            temp = pd.DataFrame()
            temp["prediction_event"] = data[data["time_stamp"]>=monthly_endtime]["prediction_event"]
            temp["time_stamp"] = pd.to_datetime(data[data["time_stamp"]>=monthly_endtime]["time_stamp"], unit='ms')
            temp.dropna(inplace=True)
            #print(temp["prediction_event"])
            #print(temp["time_stamp"])
            df = (pd.to_datetime(temp["time_stamp"]).dt.floor('d').value_counts().rename_axis('date').reset_index(name='count'))
            avg_prediction_event_monthly = df['count'].sum()/df.shape[0]
        else:
            avg_prediction_event_monthly =  np.nan

        #print("************ presence missing")
        #print("presence" in data.columns)
        #print(data.columns)
        #data.columns = data.columns.str.replace(r'\s+', '')
        try :
            print(data["presence"])
            temp = pd.DataFrame()
            temp["presence"] = data[data["time_stamp"]>=monthly_endtime]["presence"]
            temp["time_stamp"] = pd.to_datetime(data[data["time_stamp"]>=monthly_endtime]["time_stamp"], unit='ms')
            temp.dropna(inplace=True)
            df = (pd.to_datetime(temp["time_stamp"]).dt.floor('d').value_counts().rename_axis('date').reset_index(name='count'))
            avg_presence_monthly = df['count'].sum()/df.shape[0]
        except:
            print("Presence column is not present")
            avg_presence_monthly = np.nan


        print(avg_gait_velocity_monthly)
        print(avg_heart_rate_monthly)
        print(avg_respiration_rate_monthly)
        print(avg_prediction_event_monthly)
        print(avg_presence_monthly)

        # calculate weekly sensor average
        weekly_endtime = get_beginning_of_week_unix_timestamp()
        avg_gait_velocity_weekly =  data[data["time_stamp"]>=weekly_endtime]["gait_velocity"].mean() if "gait_velocity" in data.columns else np.nan
        avg_heart_rate_weekly = data[(data["time_stamp"]>=weekly_endtime) & (data["heart_rate"]!=0)]["heart_rate"].mean() if "heart_rate" in data.columns else np.nan
        avg_respiration_rate_weekly = data[(data["time_stamp"]>=weekly_endtime) & (data["respiration_rate"]!=0)]["respiration_rate"].mean() if "respiration_rate" in data.columns else np.nan

        ## Weeky prediction_event and presence sensors
        #prediction_event weekly
        if "prediction_event" in data.columns:
            temp = pd.DataFrame()
            temp["prediction_event"] = data[data["time_stamp"]>=weekly_endtime]["prediction_event"]
            temp["time_stamp"] = pd.to_datetime(data[data["time_stamp"]>=weekly_endtime]["time_stamp"], unit='ms')
            temp.dropna(inplace=True)
            df = (pd.to_datetime(temp["time_stamp"]).dt.floor('d').value_counts().rename_axis('date').reset_index(name='count'))
            avg_prediction_event_weekly = df['count'].sum()/df.shape[0]
        else:
            avg_prediction_event_weekly = np.nan

        #presence weekly
        try :
            print(data["presence"])
            temp = pd.DataFrame()
            temp["presence"] = data[data["time_stamp"]>=weekly_endtime]["presence"]
            temp["time_stamp"] = pd.to_datetime(data[data["time_stamp"]>=weekly_endtime]["time_stamp"], unit='ms')
            temp.dropna(inplace=True)
            df = (pd.to_datetime(temp["time_stamp"]).dt.floor('d').value_counts().rename_axis('date').reset_index(name='count'))
            avg_presence_weekly = df['count'].sum()/df.shape[0]
        except:
            print("Presence column is not present")
            avg_presence_weekly = np.nan


        #avg_prediction_event_weekly = data[data["time_stamp"]>=weekly_endtime]["prediction_event"].count() if "prediction_event" in data.columns else np.nan
        #avg_presence_weekly = data[data["time_stamp"]>=weekly_endtime]["presence"].count() if "presence" in data.columns else np.nan
        print(avg_gait_velocity_weekly)
        print(avg_heart_rate_weekly)
        print(avg_respiration_rate_weekly)
        print(avg_prediction_event_weekly)
        print(avg_presence_weekly)
        # insert sensor average into cache table
        result_key = {}
        result_key["user_id"] = {"S": user_id}
        # dummy value for average
        result_key["time_stamp"] = {"S": str(0)}

        # put or update item
        expression_attribute_values = {}
        expression_attribute_values[":avg_gait_velocity_monthly"] = {"S": str(avg_gait_velocity_monthly)}
        expression_attribute_values[":avg_heart_rate_monthly"] = {"S": str(avg_heart_rate_monthly)}
        expression_attribute_values[":avg_respiration_rate_monthly"] = {"S": str(avg_respiration_rate_monthly)}
        expression_attribute_values[":avg_prediction_event_monthly"] = {"S": str(avg_prediction_event_monthly)}
        expression_attribute_values[":avg_presence_monthly"] = {"S": str(avg_presence_monthly)}
        expression_attribute_values[":avg_gait_velocity_weekly"] = {"S": str(avg_gait_velocity_weekly)}
        expression_attribute_values[":avg_heart_rate_weekly"] = {"S": str(avg_heart_rate_weekly)}
        expression_attribute_values[":avg_respiration_rate_weekly"] = {"S": str(avg_respiration_rate_weekly)}
        expression_attribute_values[":avg_prediction_event_weekly"] = {"S": str(avg_prediction_event_weekly)}
        expression_attribute_values[":avg_presence_weekly"] = {"S": str(avg_presence_weekly)}



        client.update_item(
            TableName="qvntra-baseline-cache",
            Key=result_key,
            UpdateExpression="set avg_gait_velocity_monthly =:avg_gait_velocity_monthly, \
                avg_heart_rate_monthly =:avg_heart_rate_monthly, \
                avg_respiration_rate_monthly =:avg_respiration_rate_monthly, \
                avg_prediction_event_monthly =:avg_prediction_event_monthly, \
                avg_presence_monthly =:avg_presence_monthly, \
                avg_gait_velocity_weekly =:avg_gait_velocity_weekly, \
                avg_heart_rate_weekly =:avg_heart_rate_weekly, \
                avg_respiration_rate_weekly =:avg_respiration_rate_weekly, \
                avg_prediction_event_weekly =:avg_prediction_event_weekly, \
                avg_presence_weekly =:avg_presence_weekly",
            ExpressionAttributeValues = expression_attribute_values
            )


# for local invoke testing
if __name__ == "__main__":
    lambda_handler({}, {})
