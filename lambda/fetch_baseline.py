import json
import pandas as pd
import numpy as np
from helper import *


def lambda_handler(event, context):
    # get parameter
    user_id = event["queryStringParameters"].get("user_id")
    # calculate result
    client = boto3.client("dynamodb", region_name="us-east-1")
    table_name = "qvntra-baseline-cache"
    try:
        result = client.query(
                    TableName = table_name,
                    KeyConditionExpression = "user_id = :user_id",
                    ExpressionAttributeValues = {":user_id": {"S": user_id} },
                    ProjectionExpression = "user_id, time_stamp, baseline_respiration_rate, baseline_heart_rate, baseline_gait_velocity, baseline_prediction, baseline_prediction_event",
                    ScanIndexForward = False
                    )
        baseline_respiration_rate = result["Items"][0].get("baseline_respiration_rate").get("S")
        baseline_heart_rate = result["Items"][0].get("baseline_heart_rate").get("S")
        baseline_gait_velocity = result["Items"][0].get("baseline_gait_velocity").get("S")
        baseline_prediction = result["Items"][0].get("baseline_prediction").get("S")
        baseline_prediction_event = result["Items"][0].get("baseline_prediction_event").get("S")

        body = {
            "gait_velocity":float(baseline_gait_velocity),
            "heart_rate":float(baseline_heart_rate),
            "respiration_rate":float(baseline_respiration_rate),
            "prediction":float(baseline_prediction),
            "prediction_event":float(baseline_prediction_event)
        }
    
    
        return {
            "statusCode": 200,
            "headers": {'Content-Type': 'application/json'},
            "body": body
        }
    except Exception as e:
        print(e)
        return {
            "statusCode": 404,
            "headers": {'Content-Type': 'application/json'},
            "body": { "error": f"Can not get baseline for user {user_id}" },
        }


# for local invoke testing
if __name__ == "__main__":
    event = {}
    event["queryStringParameters"] = {}
    event["queryStringParameters"]["user_id"] = "thyge"
    response = lambda_handler(event, {})
    print(response["body"])