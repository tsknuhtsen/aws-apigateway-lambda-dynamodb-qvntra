import boto3
import pandas as pd
import numpy as np
from dynamodb_json import json_util
from datetime import datetime, timedelta
import time

def get_data_by_user_id(client, 
                        user_id:str, 
                        starttime:int, 
                        endtime:int,
                        projection_expression:str,
                        expression_attribute_names:dict={}):
    """ Get data for certain user_id in time window from now to last_x_hours
    """
    paginator = client.get_paginator("query")
    if expression_attribute_names:
        page_iterator = paginator.paginate(
                        TableName = "qvntra",
                        KeyConditionExpression = "user_id = :user_id and time_stamp between :endtime and :starttime",
                        ExpressionAttributeValues = {
                            ":user_id": {"S": user_id},
                            ":endtime": {"N": str(endtime)},
                            ":starttime": {"N": str(starttime)}
                        },
                        ProjectionExpression = projection_expression,
                        ExpressionAttributeNames = expression_attribute_names,
                        ScanIndexForward = False
                        )
    else:
        page_iterator = paginator.paginate(
                TableName = "qvntra",
                KeyConditionExpression = "user_id = :user_id and time_stamp between :endtime and :starttime",
                ExpressionAttributeValues = {
                    ":user_id": {"S": user_id},
                    ":endtime": {"N": str(endtime)},
                    ":starttime": {"N": str(starttime)}
                },
                ProjectionExpression = projection_expression,
                ScanIndexForward = False
                )
    items = []
    for page in page_iterator: items.extend(page["Items"])
    data = pd.DataFrame(json_util.loads(items))
    print(data)

    # pandas astype errors = "ignore" bug https://github.com/pandas-dev/pandas/issues/30324
    if "respiration_rate" in data.columns: data = data.astype({"respiration_rate":"float"})
    if "heart_rate" in data.columns: data = data.astype({"heart_rate":"float"})
    if "gait_velocity" in data.columns: data = data.astype({"gait_velocity":"float"})
    if "status" in data.columns: data = data.astype({"status":"float"})
    if "occupancy" in data.columns: data = data.astype({"occupancy":"boolean"})
    return data


def get_all_data(client):
    paginator = client.get_paginator("scan")
    page_iterator = paginator.paginate(
        TableName = "qvntra",
        ProjectionExpression = "user_id, organization, topic, time_stamp, respiration_rate, heart_rate, gait_velocity, prediction",
    )
    items = []
    for page in page_iterator:
        items.extend(page["Items"])

    data = pd.DataFrame(json_util.loads(items))
    return data


def get_all_user_ids(client):
    paginator = client.get_paginator("scan")
    page_iterator = paginator.paginate(
        TableName = "qvntra",
        ProjectionExpression = "user_id",
    )
    items = []
    for page in page_iterator:
        items.extend(page["Items"])

    data = pd.DataFrame(json_util.loads(items))
    result = data["user_id"].unique()
    return result


def get_beginning_of_day_unix_timestamp():
    """ Calculate unix time backwards to the beginning of the day
    """
    today = datetime.today()
    year = today.year
    month = today.month
    day = today.day
    beginning_of_day_unix_timestamp = int((datetime(year, month, day) - datetime(1970,1,1)).total_seconds())*1000
    return beginning_of_day_unix_timestamp

def get_beginning_of_week_unix_timestamp():
    """ Calculate unix time backwards to the beginning of the week
    """
    today = datetime.today()
    beginning_of_week = today  - timedelta(days=today.weekday() % 7)
    beginning_of_week_unix_timestamp = int((beginning_of_week - datetime(1970,1,1)).total_seconds())*1000
    return beginning_of_week_unix_timestamp

def get_beginning_of_month_unix_timestamp():
    """ Calculate unix time backwards to the beginning of the month
    """
    today = datetime.today()
    year = today.year
    month = today.month
    beginning_of_month_unix_timestamp = int((datetime(year, month, 1) - datetime(1970,1,1)).total_seconds())*1000
    return beginning_of_month_unix_timestamp
