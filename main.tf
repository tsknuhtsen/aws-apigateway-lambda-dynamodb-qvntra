data "aws_region" "current" {}
data "aws_caller_identity" "current" {}


# fetch baseline
# path_part     = "baseline"
module "fetch_baseline" {
  source = "./module-lambda"
  filename = "lambda.zip"
  function_name = "fetch_baseline"
  handler       = "fetch_baseline.lambda_handler"
  timeout       = 900
}

# fetch lastseen
# path_part     = "lastseen"
module "fetch_lastseen" {
  source = "./module-lambda"
  filename = "lambda.zip"
  function_name = "fetch_lastseen"
  handler       = "fetch_lastseen.lambda_handler"
  timeout       = 900
}


# fetch all
# path_part     = "all"
module "fetch_all" {
  source = "./module-lambda"
  filename = "lambda.zip"
  function_name = "fetch_all"
  handler       = "fetch_all.lambda_handler"
  timeout       = 900
  memory_size   = 3000
}

# fetch all average
# path_part     = "all_average"
module "fetch_all_average" {
  source = "./module-lambda"
  filename = "lambda.zip"
  function_name = "fetch_all_average"
  handler       = "fetch_all_average.lambda_handler"
  timeout       = 900
  memory_size   = 3000
}

# fetch recent payload
# fetch all
# path_part     = "recent_payload"
module "fetch_recent_payload" {
  source = "./module-lambda"
  filename = "lambda.zip"
  function_name = "fetch_recent_payload"
  handler       = "fetch_recent_payload.lambda_handler"
  timeout       = 900
  memory_size   = 3000
}



# qvntra-baseline-cache table
# attributes (user_id:S, start_time_stamp:S, baseline_gait_velocity:N, baseline_heart_rate:N, baseline_respiration_rate:N, baseline_prediction:N, baseline_fixed:B)
resource "aws_dynamodb_table" "qvntra-baseline-cache" {
  name           = "qvntra-baseline-cache"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "user_id"
  range_key      = "time_stamp"

  attribute {
    name = "user_id"
    type = "S"
  }

  attribute {
    name = "time_stamp"
    type = "S"
  }
  depends_on = [module.update_baseline, module.update_all_average]
} 

# update_baseline
module "update_baseline" {
  source = "./module-lambda"
  lambda_type = "cron"
  filename = "lambda.zip"
  function_name = "update_baseline"
  handler       = "update_baseline.lambda_handler"
  timeout       = 900
  memory_size   = 3000
  schedule_expression = "rate(1 day)"
}

# update_all_average
module "update_all_average" {
  source = "./module-lambda"
  lambda_type = "cron"
  filename = "lambda.zip"
  function_name = "update_all_average"
  handler       = "update_all_average.lambda_handler"
  timeout       = 900
  memory_size   = 3000
  schedule_expression = "rate(1 day)"
}
