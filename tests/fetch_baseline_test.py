import pandas as pd
import numpy as np
import json
import boto3
from helper import *
from dynamodb_json import json_util

def calculate_baseline(data:pd.DataFrame):
    """ Calculate baseline for a certain user_id
    """
    starttime = min(data["time_stamp"])
    endtime = starttime + 7*3600*1000
    filtered_data = data[(data["time_stamp"]>=starttime) & (data["time_stamp"]<=endtime)]
    # not all measures are included for all users
    try:
        gait_velocity_avg = filtered_data["gait_velocity"].apply(lambda x: np.nan if x==0 else x).astype("float").mean(skipna=True)
    except:
        gait_velocity_avg = np.nan
    try:
        heart_rate_avg = filtered_data["heart_rate"].apply(lambda x: np.nan if x==0 else x).astype("float").mean(skipna=True)
    except:
        heart_rate_avg = np.nan
    try:
        respiration_rate_avg = filtered_data["respiration_rate"].apply(lambda x: np.nan if x==0 else x).astype("float").mean(skipna=True)
    except:
        respiration_rate_avg = np.nan
    try:
        prediction = filtered_data["prediction"].dropna()
        prediction_count = prediction[prediction.shift() != prediction].count()
    except:
        prediction_count = np.nan
    result = {
        "starttime": starttime,
        "gait_velocity": float(gait_velocity_avg),
        "heart_rate": float(heart_rate_avg),
        "respiration_rate": float(respiration_rate_avg),
        "prediction": float(prediction_count)
    }
    return result




client = boto3.client("dynamodb", region_name="us-east-1")
all_user_ids = get_all_user_ids(client)
# calculate the difference
user_ids = all_user_ids

starttime = int(time.time())*1000
# infinite backwards into history
window_size = 10000000000
endtime = starttime - window_size * 3600000
for user_id in user_ids:
    print(user_id)
    projection_expression = "topic, time_stamp, respiration_rate, heart_rate, gait_velocity, prediction"
    data = get_data_by_user_id(client, user_id, starttime, endtime, projection_expression)
    print(data.head())
    # calculate baseline
    result = calculate_baseline(data)
    starttime = str(result["starttime"])
    baseline_gait_velocity = str(result["gait_velocity"])
    baseline_heart_rate = str(result["heart_rate"])
    baseline_respiration_rate = str(result["respiration_rate"])
    baseline_prediction = str(result["prediction"])
    # calculate if baseline fixed
    # baseline_fixed = time.current() - starttime > 7 days
    # put or update item
    expression_attribute_values = {}
    expression_attribute_values[":baseline_gait_velocity"] = {"S": baseline_gait_velocity}
    expression_attribute_values[":baseline_heart_rate"] = {"S": baseline_heart_rate}
    expression_attribute_values[":baseline_respiration_rate"] = {"S": baseline_respiration_rate}
    expression_attribute_values[":baseline_prediction"] = {"S": baseline_prediction}

    result_key = {}
    result_key["user_id"] = {"S": user_id}
    result_key["time_stamp"] = {"S": starttime}

    client.update_item(
        TableName="qvntra-baseline-cache", 
        Key=result_key,
        UpdateExpression="set baseline_gait_velocity =:baseline_gait_velocity, \
                            baseline_heart_rate =:baseline_heart_rate, \
                            baseline_respiration_rate =:baseline_respiration_rate, \
                            baseline_prediction =:baseline_prediction",
        ExpressionAttributeValues = expression_attribute_values
        )