import pandas as pd
import numpy as np
from helper import *
import json
import time
from datetime import date
from datetime import datetime
from multiprocessing import Process

"""
a = time.time()



client = boto3.client("dynamodb", region_name="us-east-1")
# get parameter
user_id = "thyge"
# calculate result
starttime = int(time.time())*1000
lastseen_window_size = 24*7*5
endtime = starttime - lastseen_window_size * 3600000
data = get_data_by_user_id(client, user_id, starttime, endtime)

print(data.head())
result = pd.DataFrame.to_json(data, orient="records")

b = time.time()
print(b-a)
"""

today = datetime.today()
year = today.year
month = today.month
day = today.day
beginning_of_day_unix_timestamp = int((datetime(year, month, day) - datetime(1970,1,1)).total_seconds())*1000
print(beginning_of_day_unix_timestamp)