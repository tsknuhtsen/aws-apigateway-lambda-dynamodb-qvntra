import pandas as pd
import numpy as np
from helper import *
import json
import time


def is_recent_payload(row:dict):
    topic = row.get("topic")
    status = row.get("status")
    prediction = row.get("prediction")
    occupancy = row.get("occupancy")
    if topic == "sensor_bedroom"and status == 1:
        return True
    if topic == "sensor_bathroom" and not pf.isna(prediction):
        return True
    if topic == "sensor_kitchen" and occupancy == True:
        return True
    if topic == "sensor_kitchen" and occupancy == True:
        return True
    else:
        return False


a = time.time()
user_id = "thyge"




client = boto3.client("dynamodb", region_name="us-east-1")
starttime = int(time.time())*1000
lastseen_window_size = 24
endtime = starttime - lastseen_window_size * 3600000

projection_expression = "topic, time_stamp, prediction, #status, occupancy"
expression_attribute_names = {"#status": "status"}

data = get_data_by_user_id(client, user_id, starttime, endtime, projection_expression, expression_attribute_names)


result = {}
for index, row in data.iterrows():
    if is_recent_payload(row): 
        result = row.to_dict()
        break

print(result)
print(data.head())

"""
if not data.empty:

    gait_velocity = get_first_nonzero_notnull_value(data["gait_velocity"])
    heart_rate = get_first_nonzero_notnull_value(data["heart_rate"])
    respiration_rate = get_first_nonzero_notnull_value(data["respiration_rate"])

    endtime = max(data["time_stamp"])
    starttime = endtime - 1*3600*1000
    prediction = data[(data["time_stamp"]>=starttime) & (data["time_stamp"]<=endtime)]["prediction"].dropna()
    prediction_count = prediction[prediction.shift() != prediction].count()

		- {"topic" = "sensor_bedroom", "status" = 1}
		- {"topic" = "sensor_bathroom", "prediction" = $ANY_VALUE}
 		- {"topic" = "sensor_kitchen", "occupancy" = true}
		- {"topic" = "sensor_livingroom", "occupancy" = true}
"""