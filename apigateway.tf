resource "aws_api_gateway_rest_api" "qvntra" {
  name        = "qvntra"
  description = "qvntra"
  body = data.template_file.api_gateway_openapi_spec.rendered
}
resource "aws_api_gateway_deployment" "qvntra" {
  depends_on  = [module.fetch_baseline, module.fetch_lastseen, module.fetch_all, module.fetch_recent_payload]
  rest_api_id = aws_api_gateway_rest_api.qvntra.id
  stage_name  = var.stage_name
}

data "template_file" "api_gateway_openapi_spec" {
  template = "${file("${path.module}/swagger.yml")}"
  vars = {
    uri_recent_payload = "arn:aws:apigateway:${data.aws_region.current.name}:lambda:path/2015-03-31/functions/arn:aws:lambda:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:function:fetch_recent_payload/invocations",
    uri_fetch_baseline = "arn:aws:apigateway:${data.aws_region.current.name}:lambda:path/2015-03-31/functions/arn:aws:lambda:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:function:fetch_baseline/invocations",
    uri_fetch_lastseen = "arn:aws:apigateway:${data.aws_region.current.name}:lambda:path/2015-03-31/functions/arn:aws:lambda:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:function:fetch_lastseen/invocations",
    uri_fetch_all_average = "arn:aws:apigateway:${data.aws_region.current.name}:lambda:path/2015-03-31/functions/arn:aws:lambda:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:function:fetch_all_average/invocations",
    uri_fetch_all = "arn:aws:apigateway:${data.aws_region.current.name}:lambda:path/2015-03-31/functions/arn:aws:lambda:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:function:fetch_all/invocations"
  }
}

# source_arn = "arn:aws:execute-api:us-east-1:568062530135:${aws_api_gateway_rest_api.test.id}/*/GET/recent_payload"
resource "aws_lambda_permission" "apigw_fetch_recent_payload" {
  statement_id  = "AllowExecutionFromAPIGatewayRecentPayload"
  action        = "lambda:InvokeFunction"
  function_name = "fetch_recent_payload"
  principal     = "apigateway.amazonaws.com"
  source_arn = "arn:aws:execute-api:us-east-1:568062530135:${aws_api_gateway_rest_api.qvntra.id}/*/GET/recent_payload"
}

resource "aws_lambda_permission" "apigw_fetch_baseline" {
  statement_id  = "AllowExecutionFromAPIGatewayBaseline"
  action        = "lambda:InvokeFunction"
  function_name = "fetch_baseline"
  principal     = "apigateway.amazonaws.com"
  source_arn = "arn:aws:execute-api:us-east-1:568062530135:${aws_api_gateway_rest_api.qvntra.id}/*/GET/baseline"
}

resource "aws_lambda_permission" "apigw_fetch_lastseen" {
  statement_id  = "AllowExecutionFromAPIGatewayLastseen"
  action        = "lambda:InvokeFunction"
  function_name = "fetch_lastseen"
  principal     = "apigateway.amazonaws.com"
  source_arn = "arn:aws:execute-api:us-east-1:568062530135:${aws_api_gateway_rest_api.qvntra.id}/*/GET/lastseen"
}

resource "aws_lambda_permission" "apigw_fetch_all_average" {
  statement_id  = "AllowExecutionFromAPIGatewayAllAverage"
  action        = "lambda:InvokeFunction"
  function_name = "fetch_all_average"
  principal     = "apigateway.amazonaws.com"
  source_arn = "arn:aws:execute-api:us-east-1:568062530135:${aws_api_gateway_rest_api.qvntra.id}/*/GET/all_average"
}

resource "aws_lambda_permission" "apigw_fetch_all" {
  statement_id  = "AllowExecutionFromAPIGatewayAll"
  action        = "lambda:InvokeFunction"
  function_name = "fetch_all"
  principal     = "apigateway.amazonaws.com"
  source_arn = "arn:aws:execute-api:us-east-1:568062530135:${aws_api_gateway_rest_api.qvntra.id}/*/GET/all"
}